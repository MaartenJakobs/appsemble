Add support for replacing existing login and register pages with custom pages by naming the pages
`Login` or `Register`.
